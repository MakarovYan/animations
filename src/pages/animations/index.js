// import logo from './logo.svg';
import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import styles from './Animations.module.css';
import { randomInteger } from '../../helpers';

// const set1 = [...Array(20).keys()];
const set2 = [...Array(10).keys()];
const set3 = [...Array(7).keys()];

// const initialFlies = [];
const initialWalking = [];
const initialTopRight = [];

// set1.forEach((item) => {
//   initialFlies.push({
//     value: randomInteger(1, 9),
//     top: `${randomInteger(0, 500)}px`,
//     left: `${randomInteger(0, 90)}%`,
//     animationDelay: `${randomInteger(0, 10000)}ms`,
//   });
// });
set2.forEach((item) => {
  initialWalking.push({
    value: `P001-0653141823-4303${randomInteger(727, 999)}`,
    top: `${randomInteger(0, 900)}px`,
    left: `${randomInteger(0, 50)}%`,
    animationDuration: `${randomInteger(15, 40)}s`,
    animationDelay: `${randomInteger(0, 10000)}ms`,
  });
});
set3.forEach((item) => {
  initialTopRight.push({
    value: `P001-0653141823-4303${randomInteger(727, 999)}`,
    top: `${randomInteger(600, 1500)}px`,
    left: `${randomInteger(0, 30)}%`,
    animationDuration: `${randomInteger(0, 3)}s`,
    animationDelay: `${randomInteger(0, 10000)}ms`,
  });
});

function App() {
  const [items, setItems] = useState([]);

  function createItem() {
    return items.map((item) => (
      <div
        className={clsx(styles.element, styles.flyAway)}
        style={{
          top: item.top,
          left: item.left,
        }}
      >
        {item.value}
      </div>
    ));
  }

  // const handleClick = () => {
  //   setItems((prev) => [
  //     ...prev,
  //     {
  //       value: randomInteger(0, 500),
  //       top: `${randomInteger(0, 500)}px`,
  //       left: `${randomInteger(0, 90)}%`,
  //     },
  //   ]);
  // };

  // const handleKeypress = () => {
  //   console.log("handleKeypress");
  // };

  useEffect(() => {
    document.onkeydown = function (evt) {
      evt = evt || window.event;
      setItems((prev) => [
        ...prev,
        {
          value: evt.key.toString(),
          top: `${randomInteger(0, 500)}px`,
          left: `${randomInteger(0, 90)}%`,
        },
      ]);
    };
  }, []);

  return (
    <div>
      <section className={styles.canvas}>
        {/* {createItem()}
        {initialFlies.map((item) => (
          <div
            className={clsx(styles.element, styles.flyAway)}
            style={{
              top: item.top,
              left: item.left,
              animationDelay: item.animationDelay,
              animationIterationCount: "infinite", //
            }}
          >
            {item.value}
          </div>
        ))} */}
        {initialWalking.map((item) => (
          <div
            className={clsx(styles.element, styles.walkAround)}
            style={{
              fontSize: 18,
              top: item.top,
              left: item.left,
              animationDuration: item.animationDuration,
              animationDelay: item.animationDelay,
              animationIterationCount: 'infinite', //
            }}
          >
            {item.value}
          </div>
        ))}

        {/* {initialTopRight.map((item) => (
          <div
            className={clsx(styles.element, styles.topRight)}
            style={{
              fontSize: 18,
              top: item.top,
              left: item.left,
              animationDuration: item.animationDuration,
              animationDelay: item.animationDelay,
              animationIterationCount: "infinite", //
            }}
          >
            {item.value}
          </div>
        ))} */}
      </section>
    </div>
  );
}

export default App;
