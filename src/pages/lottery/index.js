import React, { useContext, useEffect } from 'react';
import { MainContext } from 'context';
import Input from 'components/Input';
import Wheel from 'components/Wheel2';
import Congrats from 'components/Congrats';
import { getData } from 'helpers';
import classes from './Lottery.module.css';

export default function Lottery() {
  const [store, dispatch] = useContext(MainContext);

  const { uid } = store;

  useEffect(() => {
    const getUID = () =>
      getData({
        operationName: 'RequestedRPGUIdLottery',
        queryString: `RequestedRPGUIdLottery {
      RequestedRPGUIdLottery {
        customClaimNumber
        userId
        createdAt
        updatedAt
      }
    }`,
      }).then((res) => {
        dispatch({
          type: 'SET_UID',
          payload: res.data.RequestedRPGUIdLottery.customClaimNumber,
        });
      });
    getUID();
    let timerId = setTimeout(function tick() {
      getUID();
      timerId = setTimeout(tick, 5000);
    }, 5000);
    return () => clearTimeout(timerId);
  }, [dispatch]);

  return (
    <div className={classes.root}>
      <div className={classes.heading}>
        <h1 className={classes.title}>РОЗЫГРЫШ КВАРТИРЫ</h1>
        <h2 className={classes.subtitle}>20 564 УЧАСТНИКОВ</h2>
        {/* <input type="text" value={uid} onChange={handleChange} /> */}
        {uid.length === 21 && <Congrats />}
      </div>
      <div className={classes.placeholder} />
      {uid.length !== 21 && <Wheel />}
      <Input />
    </div>
  );
}
