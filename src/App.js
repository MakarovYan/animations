import React from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import Layout from 'components/Layout';
import { MainContextProvider } from './context';
import Lottery from './pages/lottery';

export const USER_TOKEN = 'user_token';

function App() {
  return (
    <MainContextProvider>
      <Layout>
        <div>&#129300;</div>
        {/* <Router>
          <Switch>
            <Route path="/" exact component={Lottery} />
            <Route path="/lottery" exact component={Lottery} />
          </Switch> */}
        {/* </Router> */}
      </Layout>
    </MainContextProvider>
  );
}

export default App;
