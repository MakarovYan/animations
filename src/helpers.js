export function randomInteger(min, max) {
  const rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}

export const API_ENDPOINT = process.env.REACT_APP_DATA_PROVIDER;

export const getData = ({ operationName, queryString }) =>
  fetch(`${API_ENDPOINT}/graphql`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      // Authorization: `Bearer ${ls.get(USER_TOKEN)}`,
      // 'Front-End-Version': process.env.REACT_APP_VERSION_DATE,
    },
    // body: JSON.stringify({ query: '{ posts { title } }' }),
    body: JSON.stringify({ operationName, query: `query ${queryString}` }),
  }).then((res) => res.json());
