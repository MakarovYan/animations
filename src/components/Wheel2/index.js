import React, { useContext } from 'react';
import { animated, useSpring, useSprings } from 'react-spring';
import styled from 'styled-components';
// import clsx from 'clsx';
import { v4 as uuidv4 } from 'uuid';
import { MainContext } from '../../context';
import classes from './Wheel.module.css';
import { offsetFrom, offsetTo, scaleFrom, scaleTo, getZIndex } from './libs';

const AnimatedItem = styled(animated('div'))`
  position: absolute;
  margin: 0 10px;
  width: 150px;
  height: 150px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 500%;
  font-weight: 500;
  border-radius: 50%;
  /* backdrop-filter: blur(11.25px); */
  background: radial-gradient(
    circle at 100px 100px,
    rgba(255, 255, 255, 0.15),
    #000
  );
  /* background: rgba(255, 255, 255, 0.15); */
  background: ${(props) =>
    props.active ? `#b82727` : 'rgba(255, 255, 255, 0.15)'};
`;

export default function Wheel() {
  const [store] = useContext(MainContext);
  const { uid } = store;
  const stack = uid.split('').reverse();
  const placeholder = 4;
  const slots = [...Array(placeholder), ...stack];

  const uidArray = slots
    .map((digit, index) => ({
      value: digit,
      x: offsetTo(index - placeholder),
      scale: scaleTo(index - placeholder),
      // zIndex: getZIndex(index - placeholder),
      from: {
        x: offsetFrom(index - placeholder),
        scale: scaleFrom(index - placeholder),
      },
    }))
    .reverse();

  const springs = useSprings(
    uidArray.length,
    uidArray.map((item, i) => ({
      background: item.hex,
      test: item.zIndex,
      color: item.fontColor,
      opacity: 1,
      scale: item.scale,
      x: item.x,
      from: {
        x: item.from.x,
        scale: item.from.scale,
      },
    })),
  );

  return (
    <div className={classes.root}>
      <div className={classes.wheel}>
        {springs.map((props, index) => (
          <AnimatedItem
            key={uuidv4()}
            style={props}
            active={index === uidArray.length - placeholder - 1}
          >
            {uidArray[index].value || '...'}
          </AnimatedItem>
        ))}
      </div>
    </div>
  );
}
