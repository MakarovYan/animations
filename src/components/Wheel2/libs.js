const ratio = 200;
const getOffset = (param) => {
  let x = param;
  if (Math.abs(x) === 1) {
    x *= 1.1;
  }
  if (x === 0) return 0;
  const doMath = Math.floor((Math.log(Math.abs(x / 1.5) + 1) / 0.8) * ratio);
  if (x < 0) {
    return doMath;
  }
  return -doMath;
};

const getScale = (param) => {
  let x = param;
  if (param === 0) return 1;
  if (Math.abs(param) === 1) {
    x = 1.5;
  }
  const doMath = 1 / Math.abs(x);
  return doMath;
};

export const offsetFrom = (index) => getOffset(index - 1);
export const offsetTo = (index) => getOffset(index);
export const scaleFrom = (index) => getScale(index - 1);
export const scaleTo = (index) => getScale(index);

export const getZIndex = (index) => {
  switch (true) {
    case index < 0:
      return 500 + Math.abs(index);
    default:
      return null;
  }
};
