import React, { useContext } from 'react';
import clsx from 'clsx';
import { MainContext } from '../../context';
import classes from './Input.module.css';

export default function Input() {
  const [store] = useContext(MainContext);
  const { uid } = store;
  const placeholder = 21;
  const CURRENT_UID = uid;

  return (
    <div className={classes.root}>
      <h2 className={classes.title}>Номер заявки победителя</h2>
      <div className={classes.slots}>
        {[...Array(placeholder).keys()].map((i, index) => (
          <div
            key={i}
            className={clsx(
              CURRENT_UID.length - 1 === index && classes.active,

              classes.slot,
            )}
          >
            {CURRENT_UID.split('')[index]}
            {CURRENT_UID.length === index && '...'}
          </div>
        ))}
      </div>
    </div>
  );
}
