import React, { useContext } from 'react';
import clsx from 'clsx';
import { v4 as uuidv4 } from 'uuid';
import { MainContext } from '../../context';
import classes from './Wheel.module.css';

export default function Wheel() {
  const [store] = useContext(MainContext);
  const { uid } = store;
  const stack = uid.split('').reverse();
  const placeholder = 4;
  const slots = [...Array(placeholder), ...stack];

  return (
    <div className={classes.root}>
      <div className={classes.wheel}>
        {slots.map((item, index) => (
          <div
            key={uuidv4()}
            className={clsx(classes.ball, index === 4 && classes.active)}
          >
            {item || <span className={classes.hidden}>...</span>}
          </div>
        ))}
      </div>
    </div>
  );
}
