import React, { useEffect } from 'react';
import confetti from 'canvas-confetti';
import classes from './Congrats.module.css';

export default function Input() {
  useEffect(() => {
    confetti({
      angle: 40,
      spread: 70,
      particleCount: 70,
      origin: { x: 0, y: 1 },
      // colors: ["#ffffff", "#b82727"],
    });
    confetti({
      angle: 120,
      spread: 100,
      particleCount: 70,
      origin: { x: 1, y: 1 },
      // colors: ["#ffffff", "#b82727"],
    });
  }, []);

  return <div className={classes.root}>Победитель определен!</div>;
}
