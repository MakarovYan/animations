import React, { useReducer, createContext } from 'react';

const initialState = {
  uid: 'P001',
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_UID': {
      return { ...state, uid: action.payload };
    }

    default:
      return state;
  }
};

export const MainContext = createContext();

export const MainContextProvider = ({ children }) => {
  const value = useReducer(reducer, initialState);

  return <MainContext.Provider value={value}>{children}</MainContext.Provider>;
};
